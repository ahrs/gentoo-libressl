# Gentoo LibreSSL Docker Container

This repo contains a GNU Makefile useful for generating Gentoo libressl docker containers.

## Why LibreSSL?

See the [Gentoo Wiki Page](https://wiki.gentoo.org/wiki/Project:LibreSSL). 

## Why the chroot?

Docker's build system is notoriously bad. Compared to `docker run` there's no support for whitelisting/blacklisting individual kernel syscalls which means that things like PTRACE don't work properly, which causes the build to fail when preparing a stage3 tarball. The classic chroot on the other hand really doesn't care what we do. It's therefore much easier to create a chroot, prepare the base system and then create our Docker image using the special `FROM scratch` image. Our Docker image therefore becomes the following simple three lines:

```
FROM scratch

ADD chroot /

CMD ["/bin/bash"]
```

## Usage:

NOTE: This will currently **ONLY** work on my Gentoo systems. I will try to make it more generic in the future. Read the Makefile and you may be able to get it to work on your system (at a minimum you need the gentoo overlay cloned via git at `/usr/portage`, the libressl overlay cloned at `/var/lib/layman` and to copy your `make.conf` - or add a new one - to your cloned gentoo-libressl repo)

```
git clone https://gitlab.com/ahrs/gentoo-libressl.git
cd gentoo-libressl
make
```

To push the generated images to a registry you can optionally specify a docker registry as follows:

```
make REGISTRY=registry.example.com
```

Pre-built images are available on [Gitlab's Container Registry](https://gitlab.com/ahrs/gentoo-libressl/container_registry)

## To Do:

* Improve Makefile (particularly around deleting old tarballs, currently it doesn't distinguish between the multilib and nomultilib tarballs so might delete the latest multilib or nomultilib tarball when it shouldn't)
* Currently there's support for building an amd64 multilib and no multilib image using glibc. It would be nice to figure out support for alternative C libraries like uclibc and musl.
* Cross-compile - It'd be "fun" to try and cross-compile for other architectures such as ARM.
* Make the image more "minimal", strip out anything that isn't needed (e.g manpages)
