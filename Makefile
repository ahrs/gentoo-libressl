DOCKER=docker
SUDO=sudo
CHROOT=chroot
IMAGE=local/gentoo

# Base image to pull
# To Do:
# 	* Support pulling multiple base-images
BASEIMAGE=scratch

# Docker registry to push to
# To Do:
# 	* Allow pushing to multiple registries
REGISTRY?=registry.example.com

all: pull_baseimage build


FILE?=latest-stage3-amd64.txt
TAG?=gentoo-libressl:latest

all: build build_nomultilib

build_nomultilib:
	TAG=gentoo-libressl:nomultilib FILE=latest-stage3-amd64-nomultilib.txt PROFILE="default/linux/amd64/17.0/no-multilib" $(MAKE) build

build:
	rm -rf ./stage3-amd64-*
	# Fetch stage3 tarballs
	FILE=$(FILE) MIRROR=http://distfiles.gentoo.org/releases/amd64/autobuilds $(MAKE) fetch_file
	FILE="$$(tail -1 $(FILE) | awk '{print $$1}')" MIRROR=http://distfiles.gentoo.org/releases/amd64/autobuilds $(MAKE) fetch_file
	
	# Remove old stage3 tarballs
	#ls -1 | grep -E 'stage3-amd64-.*' | sed '$$d' | grep -v "$(FILE)" | xargs -r rm -rf

	# Unmount the chroot (in case it's already mounted)
	$(MAKE) umountChroot

	# Remove any leftover files from previous chroots
	rm -rf chroot
	mkdir -p chroot

	# Extract the stage3 tarball
	tar xpf stage3-*.tar.* --xattrs-include='*.*' --numeric-owner -C 'chroot'

	# Mount our chroot
	$(MAKE) mountChroot

	mkdir -p $(PWD)/chroot/var/tmp
	cp Makefile $(PWD)/chroot/var/tmp
	emerge -q --sync &> /dev/null || true
	git clone --depth=1 file:///usr/portage/.git $(PWD)/chroot/usr/portage
	cp -L /etc/resolv.conf $(PWD)/chroot/etc
	cp make.conf $(PWD)/chroot/etc/portage/make.conf
	cp make.conf.orig $(PWD)/chroot/etc/portage/make.conf.orig
	cp -R $(PWD)/etc/portage/repos.conf $(PWD)/chroot/etc/portage
	mkdir -p $(PWD)/chroot/var/lib/layman
	layman -q -s libressl || true
	git clone --depth=1 file:///var/lib/layman/libressl/.git $(PWD)/chroot/var/lib/layman/libressl
	$(SUDO) $(CHROOT) $(PWD)/chroot bash -c "cd /var/tmp && make PROFILE=$(PROFILE) inChroot"
	$(MAKE) umountChroot
	rm $(PWD)/chroot/etc/resolv.conf
	rm -rf $(PWD)/chroot/var/tmp/*
	rm -rf $(PWD)/chroot/tmp/*
	TAG=$(TAG) DOCKERFILE=Dockerfile $(MAKE) build_image
	TAG=$(TAG) $(MAKE) tag_image
	TAG=$(TAG) $(MAKE) push_image
PROFILE?="default/linux/amd64/17.0"
inChroot:
	set -e
	eselect profile set "$(PROFILE)" && \
	echo 'USE="$${USE} libressl"' >> /etc/portage/make.conf && \
	FEATURES="-sandbox -usersandbox" \
	CHOST="x86_64-pc-linux-gnu" \
	CFLAGS="-O2 -pipe -march=x86-64" \
	CXXFLAGS="-O2 -pipe -march=x86-64" \
	MAKEOPTS="-j$$[$$(nproc)+1]" \
	CURL_SSL="libressl" emerge -vuDN --binpkg-respect-use=n --usepkg y dev-vcs/git app-portage/layman @world --exclude="sys-libs/sandbox sys-libs/glibc" && \
	emerge -v --depclean || true && \
	yes 'y' | etc-update --automode -3 || true && \
	rm -rf /usr/portage && \
	rm -rf /var/lib/layman/libressl && \
	rm -rf /var/cache/edb/binhost && \
	rm -rf /var/tmp/portage/* && \
	find / -name "__pycache__" -exec rm -rf "{}" \; 2> /dev/null || true && \
	grep -E '^USE=' /etc/portage/make.conf | tee -a /etc/portage/make.conf.orig && \
	mv /etc/portage/make.conf.orig /etc/portage/make.conf
mountChroot:
	$(SUDO) mount -t proc /proc $(PWD)/chroot/proc
	$(SUDO) mount --rbind /sys  $(PWD)/chroot/sys
	$(SUDO) mount --make-rslave $(PWD)/chroot/sys
	$(SUDO) mount --rbind /dev  $(PWD)/chroot/dev
	$(SUDO) mount --make-rslave $(PWD)/chroot/dev
	$(SUDO) mkdir -p $(PWD)/chroot/var/tmp
	$(SUDO) mount --bind /var/tmp $(PWD)/chroot/var/tmp

umountChroot:
	$(SUDO) umount -f -l -r $(PWD)/chroot/proc || true
	$(SUDO) umount -f -l -r $(PWD)/chroot/sys  || true
	$(SUDO) umount -f -l -r $(PWD)/chroot/dev  || true
	$(SUDO) umount -f -l -r $(PWD)/chroot/var/tmp || true

fetch_file:
	wget -N --no-if-modified-since "$(MIRROR)/$(FILE)"

pull_baseimage:
	@if [ "$(BASEIMAGE)" != "scratch" ]; then\
		$(SUDO) $(DOCKER) pull $(BASEIMAGE); \
	fi


DOCKERFILE ?= Dockerfile
TAG?=gentoo-libressl
build_image:
	$(SUDO) $(DOCKER) build --no-cache -t local/$(TAG) . -f $(DOCKERFILE)
tag_image:
	@if [ "$(REGISTRY)" != "registry.example.com" ]; then\
		$(SUDO) $(DOCKER) tag local/$(TAG) $(REGISTRY)/$(TAG); \
	fi
push_image:
	@if [ "$(REGISTRY)" != "registry.example.com" ]; then\
		$(SUDO) $(DOCKER) push $(REGISTRY)/$(TAG); \
	fi
